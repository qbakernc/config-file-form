﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPF_Fundamentals.Sources;
namespace WPF_Fundamentals
{
    class MyProgram
    {
        private static MyProgram instance;
       
        public static MyProgram Instance 
        {
            get 
            {
                if(instance == null)
                {
                    instance = new MyProgram();
                }
                return instance;
            }   
        }
        //Comments to testaaaaa
        public SourceSection MySourceSection { get; set; }
        public void TestMethod()
        {
            MySourceSection.sourceSevenCB.SelectedIndex = MySourceSection.sourceSixCB.SelectedIndex;
        }
        private MyProgram()
        {
 
        }
    }
}
