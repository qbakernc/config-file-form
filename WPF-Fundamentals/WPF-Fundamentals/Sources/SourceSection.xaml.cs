﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_Fundamentals.Sources
{
    /// <summary>
    /// Interaction logic for SourceSection.xaml
    /// </summary>
    public partial class SourceSection : UserControl
    {
        private ComboBox[] sourceArray;
        private ComboBox[] sourceInputArray;
        private ComboBox[] sourceSubArray;
        private RadioButton[] sourceTotalArray;
        public SourceSection()
        {
            InitializeComponent();
            sourceArray = new ComboBox[]{sourceOneCB, sourceTwoCB, sourceThreeCB, sourceFourCB,
                                         sourceFiveCB, sourceSixCB, sourceSevenCB, sourceEightCB };
            sourceInputArray = new ComboBox[] {sourceOneInput, sourceTwoInput, sourceThreeInput, sourceFourInput,
                                                sourceFiveInput, sourceSixInput, sourceSevenInput, sourceEightInput };
            sourceSubArray = new ComboBox[] {sourceOneSubCB, sourceTwoSubCB, sourceThreeSubCB, sourceFourSubCB,
                                                sourceFiveSubCB, sourceSixSubCB, sourceSevenSubCB, sourceEightSubCB};
            sourceTotalArray = new RadioButton[] {sourceOneRB, sourceTwoRB, sourceThreeRB, sourceFourRB,
                                                    sourceFiveRB, sourceSixRB, sourceSevenRB, sourceEightRB };
            // Registers the Radio buttons for the click event.
            foreach(RadioButton rb in sourceTotalArray)
                rb.Checked += Rb_Checked;
            MyProgram.Instance.MySourceSection = this;
        }

        private void Rb_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton totalSourcesRB = sender as RadioButton;
            int totalSources = int.Parse(totalSourcesRB.Content.ToString());
            for (int i = 0; i < totalSources; i++)
            {
                sourceArray[i].IsEnabled = true;
                sourceInputArray[i].IsEnabled = true;
                sourceSubArray[i].IsEnabled = true;
            }
            for (int i = (int)totalSources; i < 8; i++)
            {
                sourceArray[i].IsEnabled = false;
                sourceArray[i].SelectedIndex = 0;

                sourceInputArray[i].IsEnabled = false;
                sourceInputArray[i].SelectedIndex = 0;

                sourceSubArray[i].IsEnabled = false;
                sourceSubArray[i].SelectedIndex = 0;
            }
        }


    }
}
