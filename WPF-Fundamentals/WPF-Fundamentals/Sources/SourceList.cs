﻿using System;
using System.Collections.Generic;
using System.Windows.Markup;

namespace WPF_Fundamentals.Sources
{
    class SourceList : MarkupExtension
    {
        private string arrayType;
        public List<string> sources;
        public List<string> sourceInput;
        public List<string> sourceSubPage;
        public SourceList(string arrayType)
        {
            this.arrayType = arrayType;

            sources = new List<string>(){ "NinerNet PC", "CCI PC", "Mosaic PC", "Mac", "DVD", "VCR",
                                            "Blu-ray", "Doc Cam", "Laptop", "Laptop (HDMI)", "Laptop (VGA)",
                                            "PS4", "Clickshare", "Wireless", "Memorial Hall"};
            sourceInput = new List<string>() { "1", "2", "3", "4", "5", "6", "7", "8" };
            sourceSubPage = new List<string>() { "None","IP Cam Presets", "DVD", "VCR", 
                                                    "Doc Cam (UF80)", "Doc Cam (Qomo)", "Blu-ray"};
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            List<string> tempList = new List<string>();
            switch (arrayType)
            {
                case "1":
                    tempList = sources;
                    break;
                case "2":
                    tempList = sourceInput;
                    break;
                case "3":
                    tempList = sourceSubPage;
                    break;
            }
            return tempList;
        }
    }
}
