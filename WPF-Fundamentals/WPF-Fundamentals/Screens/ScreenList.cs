﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace WPF_Fundamentals.Screens
{
    class ScreenList : MarkupExtension
    {
        public List<string> Relays;
        public ScreenList()
        {
            Relays = new List<string>() { "1","2","3","4","5","6","7","8"};
        }
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return Relays;
        }
    }
}
